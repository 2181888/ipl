﻿using UnityEngine;

public class ColorHelper
{
	public static Color Red => GetColorFromHex("#e91d39");
	public static Color Purple => GetColorFromHex("#8e559e");
	public static Color Turquoise => GetColorFromHex("#017866");
	public static Color Green => GetColorFromHex("#8bbf40");
	public static Color Blue => GetColorFromHex("#019bd6");
	public static Color Orange => GetColorFromHex("#f07021");
	public static Color Yellow => GetColorFromHex("#facf00");
	public static Color LightGray => GetColorFromHex("#606060");
	public static Color LighterGray => GetColorFromHex("#b0a89f");
	public static Color WhiteGray => GetColorFromHex("#dadada");
	public static Color DarkGray => GetColorFromHex("#303030");
	public static Color DarkerGray => GetColorFromHex("#272727");
	public static Color White => GetColorFromHex("#FFFFFF");
	public static Color Black => GetColorFromHex("#000000");

	public static Color GetColorFromHex(string hexColor)
	{
		return ColorUtility.TryParseHtmlString(hexColor, out Color color) ? color : Color.magenta;
	}
}