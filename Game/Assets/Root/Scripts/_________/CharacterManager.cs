﻿using PNLib.Attributes;
using PNLib.Utility;
using UnityEngine;

public class CharacterManager : MonoBehaviour
{
	[SerializeField]
	private Character character;

	public void UpgradeMovement(int value)
	{
		character.MoveSpeed.AddModifier(new StatsModifier(value, StatsModifierType.Addition));
	}

	public void UpgradeAttack(int value)
	{
		character.AttackSpeed.AddModifier(new StatsModifier(value, StatsModifierType.Addition));
	}

	public void SwapPattern(int value)
	{
		character.SetWeapon(value);
	}

	public void Create()
	{
		throw new System.NotImplementedException();
	}

	public void Reset()
	{
		throw new System.NotImplementedException();
	}

	public void SetReward(object lastReward)
	{
		throw new System.NotImplementedException();
	}

	public void InitializeCharacter()
	{
		throw new System.NotImplementedException();
	}
}