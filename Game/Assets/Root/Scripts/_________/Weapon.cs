﻿using UnityEngine;

public class Weapon : MonoBehaviour
{
	[SerializeField]
	private WeaponData data;
	
	[SerializeField]
	private Transform firePoint;

	[SerializeField]
	private ParticleSystem muzzleParticles;

	[SerializeField]
	private Projectile projectilePrefab;

	[SerializeField]
	private float projectileSpeed = 12f;

	[SerializeField]
	private SoundType shootingSound = SoundType.Shoot;

	private float nextFireTime;

	public bool TryShoot(Color color = default(Color))
	{
		if (Time.time < nextFireTime)
			return false;

		SoundHelper.Play(shootingSound);
		nextFireTime = Time.time + data.AttackSpeed;
		muzzleParticles.Play();
		Projectile projectile = Instantiate(projectilePrefab, firePoint.position, firePoint.rotation);
		projectile.Shoot(projectileSpeed, gameObject.layer, color);
		return true;
	}

	public void SetData(WeaponData data)
	{
	}
}