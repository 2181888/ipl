﻿using UnityEngine;

namespace _
{
	public class UpgradeData : ScriptableObject
	{
		public UpgradeType Type = UpgradeType.Attack;
		public int Value = 1;
		public string Name = "[Name]";
		public string Description = "[Description]";
	}
}