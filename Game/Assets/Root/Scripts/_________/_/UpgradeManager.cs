﻿namespace _
{
	public class UpgradeManager
	{
		private CharacterManager characterManager;

		public void AddUpgrade(UpgradeData data)
		{
			switch (data.Type)
			{
				case UpgradeType.Movement:
					characterManager.UpgradeMovement(data.Value);
					break;
				case UpgradeType.Attack:
					characterManager.UpgradeAttack(data.Value);
					break;
				case UpgradeType.Pattern:
					characterManager.SwapPattern(data.Value);
					break;
			}
		}
	}
}