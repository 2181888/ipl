using PNLib.Utility;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleEffect : MonoBehaviour
{
	private ParticleSystem particlesSystem;

	private void Awake()
	{
		particlesSystem = GetComponent<ParticleSystem>();
	}

	private void OnEnable()
	{
		gameObject.AddComponent<DestroyAfterSeconds>().Seconds = particlesSystem.main.duration;
	}

	public void SetColor(Color color)
	{
		ParticleSystem.MainModule main = particlesSystem.main;
		main.startColor = color;
	}
}