using TMPro;
using UnityEngine;

public class HighScoreDisplay : MonoBehaviour
{
	[SerializeField]
	private TMP_Text highScoreText;

	private void Start()
	{
		Display(PlayerPrefs.GetInt("HighScore", 0));
	}

	private void Display(int value)
	{
		highScoreText.SetText(value.ToString());
	}
}