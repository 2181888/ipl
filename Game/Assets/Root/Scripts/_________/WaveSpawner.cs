using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PNLib.Utility;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class WaveSpawner : MonoSingleton<WaveSpawner>
{
	[SerializeField]
	private float alertDuration;

	[SerializeField]
	private SpawnAlert alertPrefab;

	[SerializeField]
	private WaveData[] arenaWaves;

	[SerializeField]
	private ParticleEffect spawnFX;

	[SerializeField]
	private Vector2 spawnOffset = new Vector2(14, 7);

	public int WaveAmount => arenaWaves.Length;
	public static event Action OnStageClearEvent;
	public static event Action<int> OnWaveStartedEvent;
	public static readonly List<Enemy> ActiveEnemies = new List<Enemy>();
	private const float IntervalBetweenSpawn = 0.2f;
	private int waveIndex = -1;
	private Coroutine wavesRoutine;

	private void Awake()
	{
		ActiveEnemies.Clear();
	}

	private void Start()
	{
		GameManager.GetInstance().OnPlayingChangedEvent += HandlePlayingChange;
	}

	private void OnDestroy()
	{
		GameManager.GetInstance().OnPlayingChangedEvent -= HandlePlayingChange;
		StopCoroutine(wavesRoutine);
	}

	private void HandlePlayingChange(bool value)
	{
		if (value)
			StartNextWave();
		else
			StopCoroutine(wavesRoutine);
	}

	private IEnumerator SpawnWavesRoutine()
	{
		WaveData wave = arenaWaves[waveIndex];
		yield return AlertWaveRoutine(wave);

		foreach (EnemyTransform enemySpawn in wave.EnemySpawns)
		{
			yield return new WaitForSeconds(IntervalBetweenSpawn);

			Instantiate((Object) enemySpawn.Enemy, enemySpawn.SpawnPosition * spawnOffset, Quaternion.identity);

			Instantiate(
					spawnFX,
					(enemySpawn.SpawnPosition * spawnOffset) + Random.insideUnitCircle,
					Quaternion.identity
				)
				.SetColor(ColorHelper.Red);

			SoundHelper.Play(SoundType.Spawn);
		}

		while (ActiveEnemies.Count > 0)
		{
			yield return null;
		}

		StartNextWave();
	}

	private IEnumerator AlertWaveRoutine(WaveData wave)
	{
		IEnumerable<Vector2> distinctSpawnPoints = Enumerable.Distinct<Vector2>(wave.EnemySpawns.Select(x => x.SpawnPosition));

		foreach (Vector2 position in distinctSpawnPoints)
		{
			Instantiate(alertPrefab, position * spawnOffset, Quaternion.identity).Trigger(alertDuration);
		}

		SoundHelper.Play(SoundType.SpawnMark);
		yield return new WaitForSeconds(alertDuration);
	}

	private void StartNextWave()
	{
		waveIndex++;

		if (waveIndex > (arenaWaves.Length - 1))
		{
			CompleteArena();
			OnStageClearEvent?.Invoke();
		}
		else
		{
			wavesRoutine = StartCoroutine(SpawnWavesRoutine());
			OnWaveStartedEvent?.Invoke(waveIndex);
		}
	}

	private void CompleteArena()
	{
	}
}