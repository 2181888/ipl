﻿using System;
using PNLib.Attributes;
using PNLib.Utility;
using UnityEngine;

[RequireComponent(typeof(Movement), typeof(Weapon), typeof(Health))]
public class Character : MonoBehaviour, IRedirectable
{
	public Stats AttackSpeed { get; set; }

	[SerializeField]
	private GraphicsHolder graphics;

	public Stats MoveSpeed { get; set; }

	[SerializeField]
	private WeaponData[] weapons;

	public static event Action OnPlayerDiedEvent;
	private bool canMove;
	private GameManager gameManager;
	private Health health;
	private Movement movement;
	private Weapon weapon;

	public void Redirect(bool isVertical)
	{
		SoundHelper.Play(SoundType.Pop);
		graphics.Bounce(.3f, .3f);
		graphics.Flash();
		movement.Redirect(isVertical);
	}

	private void Awake()
	{
		health = GetComponent<Health>();
		movement = GetComponent<Movement>();
		movement.enabled = false;
		weapon = GetComponent<Weapon>();
		weapon.enabled = false;
		health.OnDiedEvent += () => OnPlayerDiedEvent?.Invoke();
	}

	private void Start()
	{
		GameManager.GetInstance().OnPlayingChangedEvent += HandlePlayingChange;
	}

	private void OnDisable()
	{
		GameManager.GetInstance().OnPlayingChangedEvent -= HandlePlayingChange;
	}

	private void Update()
	{
		if (!Helper.IsInsideCameraViewport(transform.position, transform.localScale.x))
			health.Die();

		if (!canMove)
			return;

		Move();
		Turn();

		if (weapon.TryShoot(ColorHelper.Blue))
			graphics.Bounce(.25f);
	}

	public void SetWeapon(int value)
	{
		SwapWeapon(weapons[value]);
	}

	private void SwapWeapon(WeaponData data)
	{
		weapon.SetData(data);
	}

	private void HandlePlayingChange(bool value)
	{
		movement.enabled = value;
		weapon.enabled = value;
		canMove = value;

		if (!value)
			movement.Stop();
	}

	private void Turn()
	{
		float horizontalInput = Input.GetAxisRaw("Horizontal");

		if (Mathf.Abs(horizontalInput) > 0)
			movement.Turn((int) Mathf.Sign(horizontalInput));
	}

	private void Move()
	{
		float verticalInput = Input.GetAxisRaw("Vertical");

		if (verticalInput > 0.1f && Input.GetKeyDown(KeyCode.UpArrow))
		{
			movement.Set(MoveType.Boost);
			SoundHelper.Play(SoundType.Boost);
		}

		if (verticalInput < -0.1f && Input.GetKeyDown(KeyCode.DownArrow))
		{
			movement.Set(MoveType.Break);
			SoundHelper.Play(SoundType.Break);
		}

		if (Mathf.Abs(verticalInput) < 0.1f && (Input.GetKeyUp(KeyCode.UpArrow) || Input.GetKeyUp(KeyCode.DownArrow)))
			movement.Set(MoveType.Default);

		movement.Move();
	}
}