﻿using System.Collections.Generic;
using PNLib.Audio;
using PNLib.Utility;
using UnityEngine;

public class SoundHelper : MonoSingleton<SoundHelper>
{
	[SerializeField]
	private SFXData[] sounds;

	private static readonly Dictionary<SoundType, AudioClip[]> SoundTypeToAudioClips =
		new Dictionary<SoundType, AudioClip[]>();

	protected override void Awake()
	{
		base.Awake();
		SoundTypeToAudioClips.Clear();

		foreach (SFXData data in sounds)
		{
			SoundTypeToAudioClips[data.Type] = data.AudioClips;
		}
	}

	public static void Play(SoundType soundType)
	{
		AudioClip[] audioClips = SoundTypeToAudioClips[soundType];
		SoundManager.Play(audioClips[Random.Range(0, audioClips.Length)]);
	}
}