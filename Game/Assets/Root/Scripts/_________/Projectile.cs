﻿using PNLib.Utility;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Projectile : MonoBehaviour
{
	private Rigidbody2D rb;
	private SpriteRenderer sr;

	private void Awake()
	{
		rb = GetComponent<Rigidbody2D>();
		sr = GetComponentInChildren<SpriteRenderer>();
	}

	private void Update()
	{
		if (!Helper.IsInsideCameraViewport(transform.position))
			Die();
	}

	private void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.layer != gameObject.layer && other.collider.TryGetComponent(out Health health))
		{
			SoundHelper.Play(SoundType.Hit);
			health.TakeDamage();
		}
		else
		{
			SoundHelper.Play(SoundType.WallHit);
		}

		Die();
	}

	public void Shoot(float speed, int layer, Color color = default(Color))
	{
		gameObject.layer = layer;
		rb.velocity = speed * transform.right;

		if (color != default(Color))
			sr.color = color;
	}

	private void Die()
	{
		Destroy(gameObject);
	}
}