﻿using System;
using System;
using UnityEngine;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Movement : MonoBehaviour
{
	public float BoostPercentage => boostAmount / maxBoostDuration;

	[SerializeField]
	private float boostRechargeSpeed = .2f;

	[SerializeField]
	private float boostSpeed = 8f;

	[SerializeField]
	private float breakSpeed = 8f;

	[SerializeField]
	private float maxBoostDuration = 2f;

	[SerializeField]
	public float moveSpeed = 4f;

	[SerializeField]
	private float turnSpeed = 180f;

	private float boostAmount;
	private Rigidbody2D rb;
	private MoveType type;

	private void Awake()
	{
		rb = GetComponent<Rigidbody2D>();
		boostAmount = maxBoostDuration;
	}

	private void Update()
	{
		boostAmount += Time.deltaTime * boostRechargeSpeed;
		boostAmount = Mathf.Min(boostAmount, maxBoostDuration);
	}

	public void Stop()
	{
		rb.velocity = Vector2.zero;
	}

	public void Set(MoveType type)
	{
		if (type != MoveType.Default && boostAmount < 0.1f)
			return;

		this.type = type;
	}

	public void Move()
	{
		if (boostAmount <= 0)
			type = MoveType.Default;
		else if (type != MoveType.Default)
			boostAmount -= Time.deltaTime;

		float speed = type switch
		{
			MoveType.Boost => boostSpeed,
			MoveType.Break => breakSpeed,
			MoveType.Default => moveSpeed,
			_ => throw new ArgumentOutOfRangeException(nameof(type)),
		};

		rb.velocity = transform.right * speed;
	}

	public void Turn(int direction)
	{
		transform.eulerAngles += Vector3.forward * (direction * turnSpeed * Time.deltaTime);
	}

	public void Redirect(bool isVertical)
	{
		Vector2 velocity = rb.velocity;

		if (isVertical)
			velocity.y = -velocity.y;
		else
			velocity.x = -velocity.x;

		rb.velocity = velocity;
		rb.transform.right = velocity;
		rb.MovePosition(transform.position + ((Vector3) velocity.normalized * .1f));
	}
}

public enum MoveType
{
	Default, Boost, Break,
}