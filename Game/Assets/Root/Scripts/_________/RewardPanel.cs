using System;
using System.Collections.Generic;
using System.Linq;
using PNLib.Utility;
using UnityEngine;
using UnityEngine.UI;

public class RewardPanel : MonoBehaviour
{
	[SerializeField]
	private List<RewardData> availableRewards;

	[SerializeField]
	private Image backGroundImage;

	[SerializeField]
	private RewardDisplay rewardDisplayPrefab;

	public static event Action<RewardType> OnRewardSelectedEvent;

	private void OnEnable()
	{
		Show(false);
	}

	public void Display()
	{
		Show(true);

		foreach (Transform child in transform)
		{
			Destroy(child.gameObject);
		}

		availableRewards = Helper.Shuffle(availableRewards).ToList();

		for (int i = 0; i < 3; i++)
		{
			RewardDisplay instance = Instantiate(rewardDisplayPrefab, transform);
			instance.Display(availableRewards[i]);
			instance.OnRewardSelected += HandleRewardSelected;
		}
	}

	public void Show(bool value)
	{
		backGroundImage.enabled = value;

		if (!value)
			foreach (Transform child in transform)
			{
				Destroy(child.gameObject);
			}
	}

	private void HandleRewardSelected(RewardData data)
	{
		OnRewardSelectedEvent?.Invoke(data.Type);
		Show(false);
	}
}