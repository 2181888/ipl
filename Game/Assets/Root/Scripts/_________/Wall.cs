﻿using UnityEngine;

public class Wall : MonoBehaviour
{
	[SerializeField]
	private bool isTopWall;

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (!other.TryGetComponent(out IRedirectable redirectable))
			return;

		redirectable.Redirect(isTopWall);
	}
}