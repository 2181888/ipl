using UnityEngine;

[CreateAssetMenu(fileName = "New Reward")]
public class RewardData : ScriptableObject
{
	public string Description;
	public RewardType Type;
}

public enum RewardType
{
	FasterAttackSpeed, FasterMovement, FasterTurnSpeed,
}