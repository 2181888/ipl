﻿using System;
using System;
using System.Collections.Generic;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine;

[CreateAssetMenu(fileName = "New Wave Data", menuName = "Wave Data", order = 0)]
public class WaveData : ScriptableObject
{
	public List<EnemyTransform> EnemySpawns = new List<EnemyTransform>();
}

[Serializable]
public class EnemyTransform
{
	public Enemy Enemy;
	public Vector2 SpawnPosition;
}