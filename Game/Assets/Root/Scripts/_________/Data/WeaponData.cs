﻿using System.ComponentModel;
using UnityEngine;

[CreateAssetMenu(fileName = "New Weapon", menuName = "Weapon", order = 0)]
public class WeaponData : ScriptableObject
{
	[Description("Attacks per second.")]
	public float AttackSpeed = 1;

	public Projectile ProjectilePrefab;
	
}