﻿using PNLib.Attributes;
using UnityEngine;

[CreateAssetMenu(fileName = "New Character", menuName = "Character", order = 0)]
public class CharacterData : ScriptableObject
{
	public Stats MoveSpeed;
	public Stats AttackSpeed;
	public Stats TurnSpeed;
}