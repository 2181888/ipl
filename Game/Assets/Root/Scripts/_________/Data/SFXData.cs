﻿using UnityEngine;
using UnityEngine;

[CreateAssetMenu(fileName = "New SFX Data", menuName = "SFX Data", order = 0)]
public class SFXData : ScriptableObject
{
	public AudioClip[] AudioClips;
	public SoundType Type;
}

public enum SoundType
{
	Null = 0,
	Spawn = 1,
	SpawnMark = 2,
	Boost = 3,
	Break = 4,
	Death = 5,
	Hit = 6,
	Launch = 7,
	Shoot = 8,
	WallHit = 9,
	Pop = 10,
	Victory = 11,
	Transition = 12,
}