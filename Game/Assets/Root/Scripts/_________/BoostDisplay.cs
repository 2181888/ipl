using UnityEngine;
using UnityEngine.UI;

public class BoostDisplay : MonoBehaviour
{
	[SerializeField]
	private Image image;

	[SerializeField]
	private Movement movement;

	private void Update()
	{
		image.fillAmount = movement.BoostPercentage;
	}
}