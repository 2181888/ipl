﻿using SJ13.Speed_Run;
using SJ13.Speed_Run;
using UnityEngine;
using UnityEngine;

[RequireComponent(typeof(Movement), typeof(RotateTowardsTransform), typeof(Health))]
public class Enemy : MonoBehaviour, IRedirectable
{
	[SerializeField]
	private GraphicsHolder graphics;

	private Health health;
	private Movement movement;
	private Character character;
	private RotateTowardsTransform rotateTowardsTarget;

	public void Redirect(bool isVertical)
	{
		SoundHelper.Play(SoundType.Pop);
		graphics.Bounce(.3f, .3f);
		graphics.Flash();
		movement.Redirect(isVertical);
	}

	private void Awake()
	{
		health = GetComponent<Health>();
		movement = GetComponent<Movement>();
		rotateTowardsTarget = GetComponent<RotateTowardsTransform>();
	}

	private void Start()
	{
		rotateTowardsTarget.SetTarget(FindObjectOfType<Character>().transform);
	}

	private void OnEnable()
	{
		health.OnDiedEvent += HandleDeath;
		StageManager.ActiveEnemies.Add(this);
	}

	private void OnDisable()
	{
		health.OnDiedEvent -= HandleDeath;
	}

	private void Update()
	{
		movement.Move();
	}

	private void OnCollisionEnter2D(Collision2D other)
	{
		if (other.collider.TryGetComponent(out Health otherHealth))
		{
			if (otherHealth.GetComponent<Enemy>())
				return;

			SoundHelper.Play(SoundType.Hit);
			otherHealth.TakeDamage();
		}
	}

	private void HandleDeath()
	{
		StageManager.ActiveEnemies.Remove(this);
	}
}

public interface IRedirectable
{
	void Redirect(bool isVertical);
}