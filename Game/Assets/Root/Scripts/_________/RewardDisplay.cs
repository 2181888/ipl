using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RewardDisplay : MonoBehaviour
{
	[SerializeField]
	private TMP_Text description;

	[SerializeField]
	private TMP_Text header;

	[SerializeField]
	private Button selectButton;

	public event Action<RewardData> OnRewardSelected;

	public void Display(RewardData data)
	{
		header.SetText(data.name);
		description.SetText(data.Description);

		selectButton.onClick.AddListener(
			() =>
			{
				OnRewardSelected.Invoke(data);
				selectButton.interactable = false;
			}
		);
	}
}