﻿using DG.Tweening;
using TMPro;
using UnityEngine;

public class WaveDisplay : MonoBehaviour
{
	[SerializeField]
	private TMP_Text waveText;

	private void OnEnable()
	{
		StageManager.OnWaveStartedEvent += HandleWaveStarted;
		StageManager.OnStageClearEvent += HandleStageCleared;
	}

	private void OnDisable()
	{
		StageManager.OnWaveStartedEvent -= HandleWaveStarted;
		StageManager.OnStageClearEvent -= HandleStageCleared;
	}

	private void HandleStageCleared()
	{
		waveText.color = ColorHelper.Yellow;
		waveText.transform.DOScale(Vector3.one, .5f).From(Vector3.one * 1.2f).SetEase(Ease.OutBounce);
	}

	private void HandleWaveStarted(int waveId)
	{
		waveText.SetText($"wave {waveId + 1}/{StageManager.GetInstance().WaveAmount}");
	}
}