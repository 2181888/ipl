using DG.Tweening;
using UnityEngine;

public class GraphicsHolder : MonoBehaviour
{
	[SerializeField]
	private Transform holder;

	private new Transform transform => holder;
	private SpriteRenderer[] renderers;

	private void Awake()
	{
		renderers = holder.GetComponentsInChildren<SpriteRenderer>();
	}

	public void Bounce(float bounceAmount = .1f, float duration = .2f)
	{
		transform.DOScale(Vector3.one, duration).From(Vector3.one * (1 + bounceAmount)).SetEase(Ease.InOutQuint);
	}

	public void Flash()
	{
		foreach (SpriteRenderer sr in renderers)
		{
			sr.DOKill(true);
			sr.DOColor(sr.color, .3f).From(Color.white).SetEase(Ease.InOutQuint);
		}
	}
}