using System.Collections;
using PNLib.Utility;
using UnityEngine;

public class SpawnAlert : MonoBehaviour
{
	private SpriteRenderer[] spriteRenderers;

	private void Awake()
	{
		spriteRenderers = GetComponentsInChildren<SpriteRenderer>();
	}

	private void OnDestroy()
	{
		StopAllCoroutines();
	}

	public void Trigger(float duration)
	{
		StartCoroutine(BlinkRoutine(duration / 2f));
		gameObject.AddComponent<DestroyAfterSeconds>().Seconds = duration;
	}

	private IEnumerator BlinkRoutine(float duration)
	{
		float seconds = duration / 2f;

		foreach (SpriteRenderer spriteRenderer in spriteRenderers)
		{
			spriteRenderer.enabled = true;
		}

		yield return new WaitForSeconds(seconds);

		foreach (SpriteRenderer spriteRenderer in spriteRenderers)
		{
			spriteRenderer.enabled = false;
		}

		yield return new WaitForSeconds(seconds);
		yield return BlinkRoutine(seconds);
	}
}