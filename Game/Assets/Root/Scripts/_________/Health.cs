﻿using System;
using UnityEngine;

public class Health : MonoBehaviour
{
	public event Action OnDiedEvent;

	public void TakeDamage()
	{
		Die();
	}

	public void Die()
	{
		SoundHelper.Play(SoundType.Death);
		OnDiedEvent?.Invoke();
		Destroy(gameObject);
	}
}