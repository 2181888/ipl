using DG.Tweening;
using PNLib.Utility;
using UnityEngine;

public class Muzzle : MonoBehaviour
{
	private void OnEnable()
	{
		gameObject.AddComponent<DestroyAfterSeconds>().Seconds = 1f;
	}

	public void Initialize(float duration = .2f, float size = 1f)
	{
		Vector3 fromScale = size * Vector3.one;
		transform.DOScale(Vector3.zero, .2f).From(fromScale).SetEase(Ease.InOutQuint);
	}
}