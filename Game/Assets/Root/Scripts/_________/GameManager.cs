﻿using System;
using System.Collections;
using PNLib.Audio;
using PNLib.Utility;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	[SerializeField]
	private float waitDeathTime = 1.2f;

	[SerializeField]
	private RewardPanel rewardPanel;

	public void HandleVictory()
	{
		rewardPanel.Display();
	}

	private void HandlePlayerDeath()
	{
		SetIsPlaying(false);
		StartCoroutine(ReloadGameRoutine());
	}

	private IEnumerator ReloadGameRoutine()
	{
		Helper.Slow(.1f, waitDeathTime - .1f);
		MusicManager.SetVolume(.25f, .2f);
		MusicManager.SetPitch(.8f, .2f);
		yield return new WaitForSecondsRealtime(waitDeathTime);

		GameSceneManager.Reload();
	}
}