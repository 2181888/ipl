﻿using System;
using System.Collections;
using PNLib.Audio;
using UnityEngine;

public class Processor : MonoBehaviour
{
	[SerializeField]
	private CharacterManager characterManagerPrefab;

	[SerializeField]
	private MusicManager musicManagerPrefab;

	[SerializeField]
	private SoundHelper soundHelperPrefab;

	[SerializeField]
	private StageManager stageManagerPrefab;

	private CharacterManager characterManager;
	private MusicManager musicManager;
	private RewardManager rewardManager;

	[SerializeField]
	private RewardManager rewardManagerPrefab;

	private SoundHelper soundHelper;
	private int stageIndex;
	private StageManager stageManager;

	[SerializeField]
	private StageData[] stages;

	private IEnumerator Start()
	{
		yield return InitializeRoutine();

		while (stageIndex < stages.Length)
		{
			yield return WaitForInputRoutine();
			yield return GameplayRoutine();
			yield return RewardRoutine(stageIndex);
		}

		yield return CreditsRoutine();
	}

	private IEnumerator CreditsRoutine()
	{
		throw new NotImplementedException();
	}

	private IEnumerator InitializeRoutine()
	{
		soundHelper = Instantiate(soundHelperPrefab);
		musicManager = Instantiate(musicManagerPrefab);
		musicManager.Reset();
		musicManager.SetVolume(VolumeType.Low);
		stageManager = Instantiate(stageManagerPrefab);
		rewardManager = Instantiate(rewardManagerPrefab);
		characterManager = Instantiate(characterManagerPrefab);
		characterManager.Create();
		yield return null;
	}

	private IEnumerator WaitForInputRoutine()
	{
		while (!Input.anyKeyDown)
		{
			yield return null;
		}
	}

	private IEnumerator GameplayRoutine()
	{
		characterManager.InitializeCharacter();
		stageManager.InitializeStage(stages[stageIndex]);
		yield return stageManager.StageResultRoutine();

		if (stageManager.Victory)
		{
			stageManager.Reset();
			stageIndex++;
		}
		else
		{
			yield return ResetGameplayRoutine();
		}
	}

	private IEnumerator ResetGameplayRoutine()
	{
		characterManager.Reset();
		stageManager.Reset();
		stageIndex = 0;
		yield return null;
	}

	private IEnumerator RewardRoutine(int index)
	{
		yield return rewardManager.RewardSelectRoutine(index);

		characterManager.SetReward(rewardManager.LastReward);
	}
}